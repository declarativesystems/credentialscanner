* Entropy detection (potential password/secret) - proposed rule:
```
  "susp_credential": {
    "description": "Credential may have been detected",
    "filePattern": "*",
    "regex": "[0-9a-zA-Z\\._-]{40,256}",
    "info": [
      "Credentials _usually_ take the form of long opaque strings with no ",
      "spaces, so for the moment just match these suspicious strings. There ",
      "be benefit to investigating things like Shannon Entropy to cut down on ",
      "false positives in the future. ",
      "More info: http://blog.dkbza.org/2007/05/scanning-data-for-entropy-anomalies.html"
    ]
  },
```
Generates a major nuisance because it matches innocuous strings such as 
`NET_DIM_DEFAULT_TX_CQ_MODERATION_PKTS_FROM_EQE` - some kind of AI detection is
needed to differentiate harmless identifiers from probable tokens 

* Metrics dashboard (how many times commits refused, etc)
* Global list of files to skip (.jar, .zip, .exe, .gif, etc)
* Skip some operations (eg merge)
* Project/server wide default activation of this plugin (impossible - https://jira.atlassian.com/browse/BSERV-3597
  but implemented as a plugin https://marketplace.atlassian.com/apps/1213038/repository-templates-for-bitbucket?hosting=server&tab=overview)
* Non-jira redress mechanism