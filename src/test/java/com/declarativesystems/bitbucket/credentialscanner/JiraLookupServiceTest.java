package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.sal.api.net.ResponseException;
import com.declarativesystems.bitbucket.mock.MockApplicationLinkService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class JiraLookupServiceTest {
    private JiraLookupService jiraLookupService;
    private MockApplicationLinkService mockApplicationLinkService;

    @Before
    public void setupLookupService() {
        this.mockApplicationLinkService = new MockApplicationLinkService();
        this.jiraLookupService = new JiraLookupServiceImpl(mockApplicationLinkService);
    }


    @Test(expected = ResponseException.class)
    public void testTicketMissing() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.ticketMissing();
        jiraLookupService.lookupRedressTicket(
            MockApplicationLinkService.PROJECT_KEY,
            MockApplicationLinkService.COMMIT_ID
        );
    }

    @Test
    public void testTicketRejected() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.ticketRejected();
        RedressTicket redressTicket = jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );
        assertEquals(
                "resolution is rejected",
                MockApplicationLinkService.RESOLUTION_REJECTED,
                redressTicket.getResolution()
        );
    }

    @Test
    public void testTicketApproved() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.ticketApproved();
        RedressTicket redressTicket = jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );
        assertEquals(
                "resolution is approved",
                MockApplicationLinkService.RESOLUTION_APPROVED,
                redressTicket.getResolution()
        );
    }

    @Test(expected = RuntimeException.class)
    public void testTicketDuplicated()  throws Exception{
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.ticketDuplicated();
        RedressTicket redressTicket = jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );
        assertNull(redressTicket);
    }

    @Test
    public void testTicketOpen()  throws Exception{
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.ticketOpen();
        RedressTicket redressTicket = jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );
        assertEquals(
                "resolution is approved",
                MockApplicationLinkService.RESOLUTION_UNRESOLVED,
                redressTicket.getResolution()
        );
    }

    @Test(expected = RuntimeException.class)
    public void testTicketLookupLinkDown() throws Exception{
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_NO_LINK);
        mockApplicationLinkService.ticketOpen();
        jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );

    }

    @Test(expected = ResponseException.class)
    public void testTicketLookupServerError() throws Exception{
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_ERROR);
        mockApplicationLinkService.ticketOpen();
        jiraLookupService.lookupRedressTicket(
                MockApplicationLinkService.PROJECT_KEY,
                MockApplicationLinkService.COMMIT_ID
        );

    }

    @Test
    public void testProjectLookupFound()  throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.projectFound();
        String projectId = jiraLookupService.projectId(MockApplicationLinkService.PROJECT_KEY);
        assertEquals("resolved ok", MockApplicationLinkService.PROJECT_ID, projectId);
    }

    @Test(expected = ResponseException.class)
    public void testProjectLookupMissing() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.projectMissing();
        jiraLookupService.projectId("NOTHERE");
    }

    @Test(expected = RuntimeException.class)
    public void testProjectLookupLinkDown() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_NO_LINK);
        mockApplicationLinkService.projectMissing();
        jiraLookupService.projectId("RED");
    }

    @Test(expected = ResponseException.class)
    public void testProjectLookupServerError() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_ERROR);
        mockApplicationLinkService.projectMissing();
        jiraLookupService.projectId("RED");
    }

    @Test
    public void testTicketUrlLinkUp() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        mockApplicationLinkService.projectFound();
        String ticketUrl = jiraLookupService.createRedressTicketUrl("RED", MockApplicationLinkService.COMMIT_ID);

        assertEquals(
                "generated url ok",
                "http://mock/jira:2990/jira/secure/CreateIssueDetails!init.jspa?pid=10000&issuetype=10104&summary=%5Bcredentialscanner+override%5D+deadbeef",
                ticketUrl
        );

    }

    @Test
    public void testTicketUrlLinkDown() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_NO_LINK);
        mockApplicationLinkService.projectFound();
        String ticketUrl = jiraLookupService.createRedressTicketUrl("RED", MockApplicationLinkService.COMMIT_ID);
        assertNull(ticketUrl);
    }

    @Test
    public void testJiraAvailableUp() throws Exception{
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_OK);
        assertTrue(jiraLookupService.isJiraLinkAvailable());
    }

    @Test
    public void testJiraAvailableDown() throws Exception {
        mockApplicationLinkService.setLinkStatus(MockApplicationLinkService.SERVER_NO_LINK);
        assertFalse(jiraLookupService.isJiraLinkAvailable());
    }

}
