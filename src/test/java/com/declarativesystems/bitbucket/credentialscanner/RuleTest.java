package com.declarativesystems.bitbucket.credentialscanner;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;

public class RuleTest {

    @Test
    public void testFilePatternMatch() {
        Rule rule = new Rule();
        rule.setFilePattern("*.java");
        rule.setRegex("");
        rule.setDescription("java");

        assertTrue(
            "java files matched",
            rule.ruleApplies("/src/main/java/foo.java")
        );
    }

    @Test
    public void testFilePatternMatchAndExclude() {
        Rule rule = new Rule();
        rule.setFilePattern("*.pem");
        rule.setExcludedPattern("/src/test/*");
        rule.setRegex("");
        rule.setDescription("java");
        assertFalse(rule.ruleApplies("/src/test/bar.pem"));
    }

    @Test
    public void testValidRuleReportsValid() {
        Rule rule = new Rule();
        rule.setDescription("something");
        rule.setFilePattern("something");
        rule.setRegex("something");

        assertTrue("fully loaded rule is valid", rule.isValid());
    }

    @Test
    public void testInvalidRuleReportsInvalid() {
        Rule rule = new Rule();
        assertFalse("fully blank rule is invalid", rule.isValid());

        List<String> badFields = rule.getBadFields();

        for (String field : new String[]{
                "description", "filePattern", "regex"}) {
            assertTrue(
                    field + " reported bad",
                    badFields.contains(field)
            );
        }
    }

    @Test
    public void testEmptyRegexReportedInvalid() {
        Rule rule = new Rule();
        rule.setDescription("bad regex");
        rule.setFilePattern("*.*");
        rule.setRegex("");

        assertFalse(
                "empty regex reported",
                rule.isValidRegex()
        );

        List<String> badFields = rule.getBadFields();
        assertEquals(
                "correct number of bad fields reported",
                1,
                badFields.size()
        );
        assertEquals(
                "correct name of bad fields reported",
                "regex",
                badFields.get(0)
        );
    }

    @Test
    public void testBadRegexReportedInvalid() {
        Rule rule = new Rule();
        rule.setDescription("bad regex");
        rule.setFilePattern("*.*");
        rule.setRegex("!? a bad( regex");

        assertFalse(
                "bad regex reported",
                rule.isValidRegex()
        );

        List<String> badFields = rule.getBadFields();
        assertEquals(
                "correct number of bad fields reported",
                1,
                badFields.size()
        );
        assertEquals(
                "correct name of bad fields reported",
                "regex",
                badFields.get(0)
        );
    }

    @Test
    public void testRegexMatchesText() {
        Rule rule = new Rule();
        rule.setDescription("test regex");
        rule.setFilePattern("*.*");
        rule.setRegex("football");

        assertTrue(rule.isValid());
        assertTrue(rule.matches(" football "));
    }

    @Test
    public void testRegexNoMatchText() {
        Rule rule = new Rule();
        rule.setDescription("test regex");
        rule.setFilePattern("*.*");
        rule.setRegex("football");

        assertTrue(rule.isValid());
        assertFalse(rule.matches(" fussball "));
    }
}
