package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.RequestFactory;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class GitCommandOutputHandlerTest {
    private GitCommandOutputHandler gitCommandOutputHandler;
    private RuleServiceImpl ruleService;

    @Before
    public void setupLookupService() throws Exception {
        ruleService = new RuleServiceImpl(
                mock(ApplicationProperties.class),
                mock(RequestFactory.class),
                mock(ContentService.class),
                mock(RepositoryService.class)
        );
        InputStream in = this.getClass().getResourceAsStream("/rule/simple.json");
        ruleService.loadRuleset(in);

        this.gitCommandOutputHandler = new GitCommandOutputHandler(ruleService);
    }

    @Test
    public void testOk() throws Exception {
        URL url = this.getClass().getResource("/git/ok.patch");
        InputStream targetStream = new FileInputStream(new File(url.getFile()));

        gitCommandOutputHandler.process(targetStream);
        assertEquals(
                "no violations detected",
                0,
                ((List<RuleViolation>) gitCommandOutputHandler.getOutput()).size()
        );
    }

    @Test
    public void testViolation() throws Exception {
        URL url = this.getClass().getResource("/git/violation.patch");
        InputStream targetStream = new FileInputStream(new File(url.getFile()));

        gitCommandOutputHandler.process(targetStream);
        List<RuleViolation> ruleViolations = (List<RuleViolation>) gitCommandOutputHandler.getOutput();
        assertEquals(
                "violation count for this commit",
                1,
                ruleViolations.size()
        );

        RuleViolation ruleViolation = ruleViolations.get(0);
        assertEquals("Correct file detected",
                "/test.txt",
                ruleViolation.getFile()
        );

        assertEquals("line detected",
                1,
                ruleViolation.getLine()
        );

        assertEquals(
                "football is a common default password",
                ruleViolation.getDescription()
        );

    }

    @Test
    public void testViolationFirstLineNewFile() throws Exception {
        URL url = this.getClass().getResource("/git/first_line_new_file_violation.patch");
        InputStream targetStream = new FileInputStream(new File(url.getFile()));

        gitCommandOutputHandler.process(targetStream);
        List<RuleViolation> ruleViolations = (List<RuleViolation>) gitCommandOutputHandler.getOutput();
        assertEquals(
                "violation count for this commit",
                1,
                ruleViolations.size()
        );

        RuleViolation ruleViolation = ruleViolations.get(0);
        assertEquals("Correct file detected",
                "/football",
                ruleViolation.getFile()
        );

        assertEquals("line detected",
                1,
                ruleViolation.getLine()
        );

        assertEquals(
                "football is a common default password",
                ruleViolation.getDescription()
        );

    }
}
