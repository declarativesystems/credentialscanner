package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.upm.api.util.Option;
import com.declarativesystems.atlassian.EntitlementCheck;
import com.declarativesystems.atlassian.EntitlementCheckImpl;

import com.declarativesystems.bitbucket.mock.MockPluginLicenseManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

// must use powermock to replace final method
@RunWith(PowerMockRunner.class)
@PrepareForTest(Option.class)
public class EntitlementCheckTest {

    private EntitlementCheck entitlementCheck;
    private MockPluginLicenseManager mockPluginLicenseManager;

    @Before
    public void setupEntitlementCheckTest() {
        this.mockPluginLicenseManager = new MockPluginLicenseManager();
        this.entitlementCheck = new EntitlementCheckImpl(
                mockPluginLicenseManager
        );
    }

    @Test
    public void testLicensed() {
        mockPluginLicenseManager.setLicensed(true);
        mockPluginLicenseManager.setError(false);

        assertEquals(
                EntitlementCheck.LICENSED,
                entitlementCheck.getLicenseKey()
        );
    }

    @Test
    public void testUnlicensed() {
        mockPluginLicenseManager.setLicensed(false);
        mockPluginLicenseManager.setError(false);

        assertEquals(
                EntitlementCheck.UNLICENSED,
                entitlementCheck.getLicenseKey()
        );
    }

    @Test
    public void testExpired() {
        mockPluginLicenseManager.setLicensed(true);
        mockPluginLicenseManager.setError(true);

        assertEquals(
                EntitlementCheck.INVALID,
                entitlementCheck.getLicenseKey()
        );
    }
}

