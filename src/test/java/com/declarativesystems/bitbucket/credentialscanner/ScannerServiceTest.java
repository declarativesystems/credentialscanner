package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.RequestFactory;
import com.declarativesystems.bitbucket.mock.MockGitCommandBuilderFactory;
import org.junit.Before;

import static org.mockito.Mockito.mock;

public class ScannerServiceTest {

    private ScannerService scannerService;
    private MockGitCommandBuilderFactory mockGitCommandBuilderFactory;
    private RuleService ruleService;

    @Before
    public void setupLookupService() {
        mockGitCommandBuilderFactory = new MockGitCommandBuilderFactory();
        ruleService =
            new RuleServiceImpl(
                    mock(ApplicationProperties.class),
                    mock(RequestFactory.class),
                    mock(ContentService.class),
                    mock(RepositoryService.class)
            );


        this.scannerService = new ScannerServiceImpl(
                mockGitCommandBuilderFactory,
                ruleService
        );
    }

}
