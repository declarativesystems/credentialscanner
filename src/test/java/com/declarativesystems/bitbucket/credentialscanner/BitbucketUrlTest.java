package com.declarativesystems.bitbucket.credentialscanner;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class BitbucketUrlTest {

    public final static String baseUrl = "http://localhost:7990/bitbucket";

    @Test
    public void detectsBitbucketUrls() {
        BitbucketUrl bitbucketUrl = new BitbucketUrl(
                baseUrl,
                baseUrl + "/projects/RUL/repos/rules/raw/test.json"
        );

        assertTrue(bitbucketUrl.isBitbucketUrl());
    }

    @Test
    public void detectsNonBitbucketUrls() {
        BitbucketUrl bitbucketUrl = new BitbucketUrl(
                baseUrl,
                "http://www.megacorp.com/rules.json"
        );

        assertFalse(bitbucketUrl.isBitbucketUrl());
    }

    @Test(expected = RuntimeException.class)
    public void rejectsUnknownBitbucketUrls() {
        BitbucketUrl bitbucketUrl = new BitbucketUrl(
            baseUrl,
            baseUrl +"/settings/admin"
        );
    }

    @Test
    public void parsesUrlCorrectly() {
        BitbucketUrl bitbucketUrl = new BitbucketUrl(
                baseUrl,
                baseUrl + "/projects/RUL/repos/rules/raw/foobar/test.json?at=deadbeef"
        );

        assertEquals("RUL", bitbucketUrl.getProject());
        assertEquals("rules", bitbucketUrl.getSlug());
        assertEquals("foobar/test.json", bitbucketUrl.getFilePath());
        assertEquals("deadbeef", bitbucketUrl.getRevision());
    }

    @Test
    public void decodesUrlEncodedRevision() {
        BitbucketUrl bitbucketUrl = new BitbucketUrl(
                baseUrl,
                baseUrl + "/projects/PROJECT_1/repos/credentialscanner_rules/raw/custom_rules.json?at=refs%2Ftags%2Fv1"
        );

        assertEquals("refs/tags/v1", bitbucketUrl.getRevision());
    }
}
