package com.declarativesystems.bitbucket.credentialscanner;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class RulesetTest {
    @Test
    public void testSettingsIgnore() {
        Ruleset ruleset = new Ruleset();

        Map<String, Object> settings = new HashMap<>();
        List<String> ignore = new ArrayList<>();
        ignore.add("/docs");
        ignore.add("/src/test/**");
        settings.put("ignore", ignore);
        ruleset.setSettings(settings);

        assertTrue(
            ruleset.isSettingsIgnore("/src/test/resources/shouldbeignored.pem")
        );

        assertFalse(
            ruleset.isSettingsIgnore("/src/main/resources/shouldbescanned.pem")
        );
    }
}
