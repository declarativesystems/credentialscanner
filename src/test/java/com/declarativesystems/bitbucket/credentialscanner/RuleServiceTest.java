package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.RequestFactory;

import com.google.gson.JsonSyntaxException;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


public class RuleServiceTest {

    private RuleServiceImpl ruleService;

    @Before
    public void setupRuleService() {
        ruleService = new RuleServiceImpl(
            mock(ApplicationProperties.class),
            mock(RequestFactory.class),
            mock(ContentService.class),
            mock(RepositoryService.class)
        );
    }

    @Test
    public void testRuleParse() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/simple.json");
        ruleService.loadRuleset(in);

        Ruleset ruleset = ruleService.getRuleset();
        Map<String, Rule> rules = ruleset.getRules();

        Rule football = rules.get("football");
        assertNotNull(football);
        Rule password123 = rules.get("Password123!");
        assertNotNull(password123);

        assertEquals(
                "football is a common default password",
                football.getDescription()
        );
        assertEquals(
                "*",
                football.getFilePattern()
        );
        assertEquals(
                "football",
                football.getRegex()
        );

    }

    @Test
    public void testFilePatternMatch() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/file_pattern_match.json");
        ruleService.loadRuleset(in);


        Map<String, Rule> rules = ruleService.getRulesForFile("/src/test/resources/password.txt");
        assertEquals(0, rules.size());
    }

    @Test(expected = RuntimeException.class)
    public void testBadJsonHandled() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/bad_json.json");
        ruleService.loadRuleset(in);
    }

    @Test
    public void testBadRegexHandled() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/bad_regex.json");
        try {
            ruleService.loadRuleset(in);
            fail("should have raised exception");
        } catch (RuntimeException e) {
            Map<String, Rule> invalidRules = ruleService.getInvalidRules();
            assertEquals(2, invalidRules.size());
        }
    }

    @Test
    public void testMissingDescriptionHandled() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/missing_description.json");
        try {
            ruleService.loadRuleset(in);
            fail("should have raised exception");
        } catch (RuntimeException e) {
            Map<String, Rule> invalidRules = ruleService.getInvalidRules();
            assertEquals(1, invalidRules.size());
        }
    }

    @Test
    public void testMissingFilePatternHandled() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/missing_filePattern.json");
        try {
            ruleService.loadRuleset(in);
            fail("should have raised exception");
        } catch (RuntimeException e) {
            Map<String, Rule> invalidRules = ruleService.getInvalidRules();
            assertEquals(1, invalidRules.size());
        }
    }

    @Test
    public void testMissingRegexHandled() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/rule/missing_regex.json");
        try {
            ruleService.loadRuleset(in);
            fail("should have raised exception");
        } catch (RuntimeException e) {
            Map<String, Rule> invalidRules = ruleService.getInvalidRules();
            assertEquals(1, invalidRules.size());
        }
    }

    @Test
    public void testDefaultRulesAllValid() throws Exception {
        InputStream in = this.getClass().getResourceAsStream("/default_rules.json");
        try {
            ruleService.loadRuleset(in);
        } catch (JsonSyntaxException e) {
            fail("bad json: " + e.getMessage());
        } catch (RuntimeException e) {
            // rules failed to load... why?
            e.printStackTrace();
            Map<String, Rule> invalidRules = ruleService.getInvalidRules();

            assertEquals(0, invalidRules.size());
        }
    }
}
