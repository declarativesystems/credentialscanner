package com.declarativesystems.bitbucket.servlet;

import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.util.Option;
import com.declarativesystems.atlassian.EntitlementCheck;
import com.declarativesystems.atlassian.EntitlementCheckImpl;
import com.declarativesystems.bitbucket.mock.MockPluginLicenseManager;
import com.declarativesystems.bitbucket.mock.MockUserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import static org.junit.Assert.assertEquals;


// must use powermock to replace final method
@RunWith(PowerMockRunner.class)
@PrepareForTest(Option.class)
public class LicenseServletTest extends Mockito {

    MockPluginLicenseManager mockPluginLicenseManager;
    EntitlementCheck entitlementCheck;
    @Before
    public void setup() {
        mockPluginLicenseManager = new MockPluginLicenseManager();
        entitlementCheck = new EntitlementCheckImpl(mockPluginLicenseManager);
    }

    @Test
    public void testLoggedOutNoAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        PluginLicenseManager pluginLicenseManager =  mock(PluginLicenseManager.class);//new MockPluginLicenseManagerService();
        when(pluginLicenseManager.getLicense()).thenReturn(Option.option(mock(PluginLicense.class)));

        new LicenseServlet(
                new MockUserManager(MockUserManager.LOGGED_OUT),
                entitlementCheck
        ).doGet(request, response);

        // https://stackoverflow.com/a/30133732/3441106
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoggedInUserNoAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        MockPluginLicenseManager pluginLicenseManager =  new MockPluginLicenseManager();
        pluginLicenseManager.setLicensed(true);
        new LicenseServlet(
                new MockUserManager(MockUserManager.USER),
                entitlementCheck
        ).doGet(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoggedInAdminAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        MockPluginLicenseManager pluginLicenseManager =  new MockPluginLicenseManager();
        pluginLicenseManager.setLicensed(true);
        new LicenseServlet(
                new MockUserManager(MockUserManager.ADMIN),
                entitlementCheck
        ).doGet(request, response);

        verify(response).setStatus(HttpServletResponse.SC_OK);
    }

    @Test
    public void testOutputOnLicenseOk() throws Exception{
        mockPluginLicenseManager.setLicensed(true);
        mockPluginLicenseManager.setError(false);

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        MockPluginLicenseManager pluginLicenseManager =  new MockPluginLicenseManager();
        pluginLicenseManager.setLicensed(true);
        new LicenseServlet(
                new MockUserManager(MockUserManager.ADMIN),
                entitlementCheck
        ).doGet(request, response);


        writer.flush();


        assertEquals(EntitlementCheck.LICENSED, servletBuffer.toString());
    }

    @Test
    public void testOutputOnLicenseMissing() throws Exception {
        mockPluginLicenseManager.setLicensed(false);
        mockPluginLicenseManager.setError(false);

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        MockPluginLicenseManager pluginLicenseManager =  new MockPluginLicenseManager();
        pluginLicenseManager.setLicensed(true);
        new LicenseServlet(
                new MockUserManager(MockUserManager.ADMIN),
                entitlementCheck
        ).doGet(request, response);


        writer.flush();


        assertEquals(EntitlementCheck.UNLICENSED, servletBuffer.toString());
    }

    @Test
    public void testOutputOnLicenseInvalid() throws Exception{
        mockPluginLicenseManager.setLicensed(true);
        mockPluginLicenseManager.setError(true);

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        MockPluginLicenseManager pluginLicenseManager =  new MockPluginLicenseManager();
        pluginLicenseManager.setLicensed(true);
        new LicenseServlet(
                new MockUserManager(MockUserManager.ADMIN),
                entitlementCheck
        ).doGet(request, response);


        writer.flush();


        assertEquals(EntitlementCheck.INVALID, servletBuffer.toString());
    }


}

