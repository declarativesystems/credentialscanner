package com.declarativesystems.bitbucket.servlet;

import com.declarativesystems.bitbucket.mock.MockUserManager;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

public class DefaultRulesServletTest extends Mockito {

    @Test
    public void testLoggedOutNoAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bamboo
        new DefaultRulesServlet(
                new MockUserManager(MockUserManager.LOGGED_OUT)
        ).doGet(request, response);

        // https://stackoverflow.com/a/30133732/3441106
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoggedInUserNoAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        new DefaultRulesServlet(
                new MockUserManager(MockUserManager.USER)
        ).doGet(request, response);

        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test
    public void testLoggedInAdminAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bamboo
        new DefaultRulesServlet(
                new MockUserManager(MockUserManager.ADMIN)
        ).doGet(request, response);

        verify(response).setStatus(HttpServletResponse.SC_OK);
    }

}

