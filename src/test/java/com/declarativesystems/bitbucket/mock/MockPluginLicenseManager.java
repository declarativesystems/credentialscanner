package com.declarativesystems.bitbucket.mock;

import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.util.Option;
import com.declarativesystems.bitbucket.credentialscanner.Rule;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

/**
 * Have to use PowerMock because `Option` contains a final method we need to
 * mock which is *impossible* with normal mockito
 * https://stackoverflow.com/a/12142046/3441106
 */
public class MockPluginLicenseManager implements PluginLicenseManager {
    private boolean licensed = false;
    private boolean error = false;

    public void setLicensed(boolean licensed) {
        this.licensed = licensed;
    }
    public void setError(boolean error) {
        this.error = error;
    }

    /**
     * We must use PowerMock to replace the `isDefined()` method and `doReturn`
     * to avoid calling the real methods...
     */
    @Override
    public Option<PluginLicense> getLicense() {
        Option<PluginLicense> option = PowerMockito.mock(Option.class);

        PluginLicense pluginLicense = PowerMockito.mock(PluginLicense.class);
        Option<PluginLicense> optionError = PowerMockito.mock(Option.class);
        PowerMockito.doReturn(error).when(optionError).isDefined();
        PowerMockito.doReturn(optionError).when(pluginLicense).getError();

        PowerMockito.doReturn(licensed).when(option).isDefined();
        PowerMockito.doReturn(pluginLicense).when(option).get();

        return option;
    }

    @Override
    public boolean isUserInLicenseRole(String s) {
        return false;
    }

    @Override
    public Option<Integer> getCurrentUserCountInLicenseRole() {
        return null;
    }

    @Override
    public String getPluginKey() {
        return null;
    }
}
