package com.declarativesystems.bitbucket.mock;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.mockito.Mockito.mock;

public class MockGitCommandBuilderFactory implements GitCommandBuilderFactory {
    @Nonnull
    @Override
    public GitScmCommandBuilder builder() {
        return mock(GitScmCommandBuilder.class);
    }

    @Nonnull
    @Override
    public GitScmCommandBuilder builder(@Nullable Repository repository) {
        return mock(GitScmCommandBuilder.class);
    }
}
