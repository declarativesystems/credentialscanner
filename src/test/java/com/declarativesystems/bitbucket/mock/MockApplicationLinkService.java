package com.declarativesystems.bitbucket.mock;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


public class MockApplicationLinkService implements ApplicationLinkService {

    private boolean linkStatus = false;
    public static final String PROJECT_KEY      = "RED";
    public static final String PROJECT_ID       = "10000";
    public static final String COMMIT_ID        = "deadbeef";
    public static final String RESOLUTION_APPROVED = "Done";
    public static final String RESOLUTION_REJECTED = "Won't Do";
    public static final String RESOLUTION_UNRESOLVED = null;

    public static final int SERVER_OK = 0;
    public static final int SERVER_NO_LINK = 1;
    public static final int SERVER_ERROR = 2;

    private String json;
    private int status;
    private URI jiraUrl;
    private boolean serverError;

    public void setLinkStatus(int linkStatus) throws URISyntaxException {
        switch (linkStatus) {
            case SERVER_OK: {
                jiraUrl = new URI("http://mock/jira:2990/jira");
                this.linkStatus = true;
                serverError = false;
                break;
            }
            case SERVER_NO_LINK: {
                jiraUrl = null;
                this.linkStatus = false;
                serverError = false;
                break;
            }
            case SERVER_ERROR: {
                jiraUrl = new URI("http://mock/jira:2990/jira");
                this.linkStatus = true;
                serverError = true;
                break;
            }
            default: {
                throw new RuntimeException("Invalid link status for mock: " + linkStatus);
            }
        }


    }


    public void ticketMissing() throws IOException {
        status = 404;
        readJson("/json/search/ticket_missing.json");
    }

    public void ticketDuplicated() throws IOException {
        status = 200;
        readJson("/json/search/ticket_duplicated.json");
    }

    public void ticketApproved() throws IOException {
        status = 200;
        readJson("/json/search/ticket_approved.json");
    }

    public void ticketRejected() throws IOException {
        status = 200;
        readJson("/json/search/ticket_rejected.json");
    }

    public void ticketOpen() throws IOException {
        status = 200;
        readJson("/json/search/ticket_open.json");
    }

    public void projectFound() throws IOException {
        status = 200;
        readJson("/json/projects/project_found.json");
    }

    public void projectMissing() throws IOException {
        status = 404;
        readJson("/json/projects/project_missing.json");
    }

    private void readJson(String path) throws IOException {
        URL url = this.getClass().getResource(path);
        json = FileUtils.readFileToString(new File(url.getFile()),  StandardCharsets.UTF_8.toString());
    }


    @Override
    public ApplicationLink getApplicationLink(ApplicationId applicationId) throws TypeNotInstalledException {
        return null;

    }

    @Override
    public Iterable<ApplicationLink> getApplicationLinks() {
        throw new RuntimeException("mock method not implemented");
    }

    @Override
    public Iterable<ApplicationLink> getApplicationLinks(Class<? extends ApplicationType> aClass) {
        throw new RuntimeException("mock method not implemented");
    }

    @Override
    public ApplicationLink getPrimaryApplicationLink(Class<? extends ApplicationType> aClass) {
        ApplicationLink applicationLink = null;


        if (aClass != JiraApplicationType.class) {
            throw new RuntimeException("mock only supports jira applinks");
        } else if (linkStatus){
            try {
                applicationLink = mock(ApplicationLink.class);
                ApplicationLinkRequestFactory applicationLinkRequestFactory = mock(ApplicationLinkRequestFactory.class);
                ApplicationLinkRequest applicationLinkRequest = mock(ApplicationLinkRequest.class);

                when(applicationLink.getId()).thenReturn(new ApplicationId(UUID.randomUUID().toString()));
                when(applicationLink.getDisplayUrl()).thenReturn(jiraUrl);
                when(applicationLink.createAuthenticatedRequestFactory()).thenReturn(applicationLinkRequestFactory);
                when(applicationLinkRequestFactory.createRequest(any(Request.MethodType.POST.getClass()), anyString())).thenReturn(applicationLinkRequest);
                when(applicationLinkRequest.addHeader(anyString(),anyString())).thenReturn(applicationLinkRequest);
                when(applicationLinkRequest.setRequestBody(anyString())).thenReturn(applicationLinkRequest);
                when(applicationLinkRequest.executeAndReturn(any())).thenAnswer(i -> {
                    if (serverError) {
                        throw new ResponseException("Mock server set to error");
                    }
                    Response response = mock(Response.class);
                    when(response.getStatusCode()).thenReturn(status);
                    when(response.getResponseBodyAsString()).thenReturn(json);

                    ReturningResponseHandler r = (ReturningResponseHandler) (i.getArguments()[0]);

                    return r.handle(response);
                });

            } catch(CredentialsRequiredException | ResponseException e) {
                throw new RuntimeException("Unexpected exception in mock!", e);
            }
        } else {
            return null;
        }
        return applicationLink;
    }
}
