define('credentialscanner/config', [
    'exports',
    'jquery'
], function (exports, $) {

    var noJiraIntegration = function() {
        AJS.$("#redressTicketsEnabled").prop('checked', false).attr("disabled", true);
        AJS.$("#jiraIntegrationDiv").hide();
        AJS.messages.warning('#credentialscannerMessageBar', {
            title: "Applinks not available",
            body: "Please configure primary Application Link to Jira to enable Redress Tickets"
        });
    };

    var projectListIntegration = function(entity) {
        // http://.../confluence/rest/applinks/1.0/entities/144880e9-a353-312f-9412-ed028e8166fa
        var url = AJS.contextPath() + '/rest/applinks/1.0/entities/' + entity
        var jqxhr = AJS.$.get(url, "json")
            .done(function (resp) {

                var selectedProject = AJS.$('#selectedProject').val();
                var projects = resp.firstChild.getElementsByTagName("entity");
                initSelect(AJS.$("#redressTicketProjectBox"), selectedProject);

                for (var i=0;i<projects.length;i++) {
                    var key = projects[i].getAttribute("key");
                    var name = projects[i].getAttribute("name");
                    var typeId = projects[i].getAttribute("typeId");

                    if (typeId == "jira.project") {
                        var option = AJS.$("<option>").text(name).attr("value", key);

                        if (key == selectedProject) {
                            option.attr("selected", "selected");
                        }

                        option.appendTo("#redressTicketProjectBox");
                    }
                }
            })
            .fail(function (jqXHR, textStatus) {
                AJS.messages.error('#credentialscannerMessageBar', {
                    title: textStatus,
                    body: "Unable to download <a target='_blank' href=\"" + url + "\">project list (entities)</a>: " + jqXHR.status
                });
            });

    };

    function jiraIntegration() {
        // applinks
        var jqxhr = AJS.$.get(AJS.contextPath() + '/rest/applinks/1.0/applicationlink/type/jira',null, "xml")
            .done(function (resp) {
                var applicationLinks = resp.firstChild.getElementsByTagName("applicationLinks");
                var entity = null;
                var found = false;
                for (var i=0;i<applicationLinks.length;i++) {
                    var id =        applicationLinks[i].getElementsByTagName("id")[0].childNodes[0].nodeValue;
                    var typeId=     applicationLinks[i].getElementsByTagName("typeId")[0].childNodes[0].nodeValue;
                    var isPrimary=  applicationLinks[i].getElementsByTagName("isPrimary")[0].childNodes[0].nodeValue;
                    if (typeId == "jira" && isPrimary == "true") {
                        // jira found, setup integration
                        found = true;
                        entity = id;
                        var rpcUrl = applicationLinks[i].getElementsByTagName("rpcUrl")[0].childNodes[0].nodeValue;

                        resolutionLists(rpcUrl);
                        projectListIntegration(entity);

                        if (AJS.$("#redressTicketsEnabled").prop('checked')) {
                            AJS.$('#jiraIntegrationDiv').show();
                        }

                        break;
                    }
                }
                if (! found) {
                    noJiraIntegration();
                }

            })
            .fail(noJiraIntegration);

    }

    function initSelect(selectBox, defaultValue) {
        var noSelection = "select...";
        var option = AJS.$("<option>").text(noSelection)
            .attr("value", noSelection)
            .attr("disabled", "disabled");

        if (defaultValue !== null) {
            option.attr("selected", "selected");
        }

        selectBox.empty().append(option);
    }

    function resolutionLists(rpcUrl) {
        // resolution list available as JSON using the api, no auth needed
        var url = rpcUrl + '/rest/api/2/resolution';
        var jqxhr2 = AJS.$.get(url, null, 'json')
            .done(function (resp) {
                var selectedResolutionApprove = AJS.$('#selectedResolutionApprove').val();
                var selectedResolutionReject = AJS.$('#selectedResolutionReject').val();
                initSelect(AJS.$("#resolutionApproveBox"), selectedResolutionApprove);
                initSelect(AJS.$("#resolutionRejectBox"), selectedResolutionReject);

                for (var i=0;i<resp.length;i++) {
                    var name = resp[i]["name"];

                    var approveOption = AJS.$("<option>").text(name).attr("value", name);
                    var rejectOption = AJS.$("<option>").text(name).attr("value", name);

                    if (name == selectedResolutionApprove) {
                        approveOption.attr("selected", "selected");
                    }
                    if (name == selectedResolutionReject) {
                        rejectOption.attr("selected", "selected");
                    }
                    approveOption.appendTo("#resolutionApproveBox");
                    rejectOption.appendTo("#resolutionRejectBox");
                }
            })
            .fail(function (jqXHR, textStatus) {
                AJS.messages.error('#credentialscannerMessageBar', {
                    title: textStatus,
                    body: "Unable to download <a target='_blank' href=\"" + url + "\">resolution list</a>: " + jqXHR.status
                });
            });
    }



    // export the onReady function that the javascript in the soy
    // template will call to bootstrap everything
    exports.onReady = function () {
        // Licensing info
        AJS.$.get(AJS.contextPath() + '/plugins/servlet/credentialscanner/license')
            .done(function (resp) {
                var div = AJS.$('#licenseErrorDiv');
                if (resp === "licensed") {
                    div.html('&nbsp;');
                } else {
                    div.html('<span class="aui-lozenge aui-lozenge-error">Please purchase credentialscanner</span>');
                }
            });



        AJS.$('#builtInRulesLink').attr(
            "href",
            AJS.contextPath() + "/plugins/servlet/credentialscanner/default_rules"
        );

        AJS.$('#customRulesEnabled').change(function() {
            AJS.$('#customRulesDiv').toggle();
        });

        AJS.$('#redressTicketsEnabled').change(function() {
            AJS.$('#jiraIntegrationDiv').toggle();
        });

        jiraIntegration();
    };

});