package com.declarativesystems.bitbucket.credentialscanner;

import java.io.PrintWriter;

public class ClientOutput {
    public static final String INDENT = "    ";
    private boolean verbose;
    private PrintWriter out;

    public ClientOutput(PrintWriter out, boolean verbose) {
        this.out = out;
        this.verbose = verbose;
    }

    public void verbose(int level, Object... messages) {
        if (verbose) {
            println(level, messages);
        }
    }

    public void println(int level, Object... messages) {
        print(level, messages);
        out.print("\n");
        out.flush();
    }

    public void print(int level, Object... messages) {
        int i = 0;
        while (i < level) {
            i++;
            out.print(INDENT);
        }
        for (Object message: messages) {
            out.print(message.toString());
        }
    }
}
