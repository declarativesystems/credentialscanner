package com.declarativesystems.bitbucket.credentialscanner;

import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ruleset {
    // settings is optional per ruleset
    private Map<String, Object> settings = new HashMap<>();
    private Map<String, Rule> rules;

    public Map<String, Object> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, Object> settings) {
        this.settings = settings;
    }

    public Map<String, Rule> getRules() {
        return rules;
    }

    public void setRules(Map<String, Rule> rules) {
        this.rules = rules;
    }

    public boolean isSettingsIgnore(String filename) {
        List<String> settingsIgnore = (List<String>) settings.getOrDefault("ignore", ((Object) (new ArrayList<String>())));
        boolean ignore = false;
        for (String settingIgnore: settingsIgnore) {
            if (FilenameUtils.wildcardMatch(filename, settingIgnore)) {
                ignore = true;
                break;
            }
        }
        return ignore;
    }
}
