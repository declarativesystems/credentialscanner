package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.hook.repository.CommitAddedDetails;
import com.atlassian.bitbucket.hook.repository.PreRepositoryHookCommitCallback;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.repository.Repository;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;

public class CredentialScannerCallback implements PreRepositoryHookCommitCallback {
    private RepositoryHookResult result;
    private JiraLookupService jiraLookupService;

    private ClientOutput clientOutput;
    private Repository repository;
    private ScannerService scannerService;

    private boolean redressTicketsEnabled;
    private String redressTicketProject;
    private String resolutionApprove;
    private String resolutionReject;

    /**
     * Time spent processing *all* commmits
     *
     * Pretty sure since each commit (must?) be handled sequentially there is no
     * thread safety issue here. Also this is a pure human-interest statistic so
     * not worth worrying about right now
     */
    private long duration = 0;

    public CredentialScannerCallback(ClientOutput clientOutput,
                                     Repository repository,
                                     ScannerService scannerService,
                                     JiraLookupService jiraLookupService,
                                     boolean redressTicketsEnabled,
                                     String redressTicketProject,
                                     String resolutionApprove,
                                     String resolutionReject
    ) {
        this.clientOutput = clientOutput;
        this.repository = repository;
        this.scannerService = scannerService;
        this.jiraLookupService = jiraLookupService;
        this.redressTicketsEnabled = redressTicketsEnabled;
        this.redressTicketProject = redressTicketProject;
        this.resolutionApprove = resolutionApprove;
        this.resolutionReject = resolutionReject;
    }

    @Nonnull
    @Override
    public RepositoryHookResult getResult() {
        return result;
    }

    /**
     * For each commit...
     * https://community.atlassian.com/t5/Bitbucket-questions/Reading-the-contents-of-a-file-in-a-pre-commit-hook-filter/qaq-p/662321#M28021
     */
    @Override
    public boolean onCommitAdded(@Nonnull CommitAddedDetails commitDetails) {
        long startTime = new Date().getTime();
        String commitId =  commitDetails.getCommit().getId();

        clientOutput.verbose(
                0,
                "Scanning: ",
                commitId
        );

        List<RuleViolation> commitRuleViolations = scannerService.scan(
                repository,
                commitId
        );

        JiraOverrideRequest jiraOverrideRequest = null;

        if (! commitRuleViolations.isEmpty()) {
            jiraOverrideRequest = jiraLookupService.handleJiraOverrideRequest(
                    redressTicketsEnabled,
                    clientOutput,
                    redressTicketProject,
                    resolutionApprove,
                    resolutionReject,
                    commitId
            );
        }

        boolean status = commitRuleViolations.isEmpty() || jiraOverrideRequest.isOverride();
        if (status) {
            result = RepositoryHookResult.accepted();
            duration += new Date().getTime() - startTime;
            clientOutput.println(
                    1,
                    "credentialscanner [OK] - ",
                    commitId
            );
            clientOutput.verbose(
                    1,
                    " (Elapsed time scanning: ",
                    duration,
                    "ms)"
            );
        } else {
            // this is what was rejected...
            clientOutput.println(
                    0,
                    "\nRejected changes in ",
                    commitId
            );
            for (RuleViolation ruleViolation : commitRuleViolations) {
                clientOutput.println(0, ruleViolation);
            }

            // bitbucket will internally backtrack the output stream and print
            // this FIRST
            result = RepositoryHookResult.rejected(
                    "credentialscanner [REJECTED] - " + commitId,
                    "\n" + jiraOverrideRequest.getMessage()
            );
        }

        return status;
    }

}
