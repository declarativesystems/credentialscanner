package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.Watchdog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Capture and process the output from running git.
 *
 * Since JSR annotations don't allow prototype beans, we have to create by hand
 * a new instance inside the hook otherwise output will accumulate and we will
 * not be thread-safe
 */
public class GitCommandOutputHandler implements CommandOutputHandler<List<RuleViolation>> {

    private Logger logger = LoggerFactory.getLogger(GitCommandOutputHandler.class);
    private List<RuleViolation> ruleViolations = new ArrayList<>();
    private RuleService ruleService;

    public GitCommandOutputHandler(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @Nullable
    @Override
    public List<RuleViolation> getOutput() {
        return ruleViolations;
    }

    @Override
    public void process(InputStream inputStream) {

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        Map<String, Rule> rulesForFile = null;

        try {
            int chunkStart = 0;
            int chunkPos = 0;
            String filename = null;
            while ((line = br.readLine()) != null) {

                if (line.startsWith("+++")) {
                    // detected a new file. In the case of n-way merges
                    // (a,b,c...), get the _last_ filename
                    filename = line.replaceFirst("\\+\\+\\+ \\w", "");
                    chunkStart = 0;
                    chunkPos = 0;
                    rulesForFile = ruleService.getRulesForFile(filename);
                } else if (line.startsWith("@@ ")) {
                    // chunk offsets -
                    // @@ -A,B +C[,D] @@
                    // A - Insertion point, original file
                    // B - Lines changed
                    // C - Insertion point new file
                    // D - Lines changed(optional)
                    // eg
                    //  @@ -1,4 +1,4 @@
                    //  @@ -0,0 +1 @@
                    chunkStart = Integer.parseInt(
                            line
                                    .split("\\+")[1]
                                    .split(",")[0]
                                    .replace(" @@", "")
                    );
                } else if (line.startsWith("+")) {

                    for (Map.Entry<String, Rule> entry : rulesForFile.entrySet()) {
                        String key = entry.getKey();
                        Rule rule = entry.getValue();

                        if (rule.matches(line)) {
                            logger.debug("Rule {} Matched {}", key, line);
                            RuleViolation ruleViolation = new RuleViolation();
                            ruleViolation.setKey(key);
                            ruleViolation.setText(line);
                            ruleViolation.setFile(filename);
                            ruleViolation.setLine(chunkStart + chunkPos);
                            ruleViolation.setDescription(rule.getDescription());


                            ruleViolations.add(ruleViolation);
                        }
                    }
                    chunkPos++;
                } else if (line.startsWith("-")) {
                    // skip context deleted
                } else {
                    chunkPos++;
                }
            }
        } catch (IOException e) {
        }
    }

    @Override
    public void complete() {

    }

    @Override
    public void setWatchdog(Watchdog watchdog) {

    }
}
