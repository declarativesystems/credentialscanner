package com.declarativesystems.bitbucket.credentialscanner;

public class RuleViolation {
    private String  file        = "";
    private int     line        = -1;
    private String  key         = "";
    private String  description = "";
    private String  text        = "";

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        return new StringBuilder()
                .append(ClientOutput.INDENT)
                    .append(getFile()).append(":").append(getLine()).append("\n")
                .append(ClientOutput.INDENT).append(ClientOutput.INDENT)
                        .append(getText()).append("\n")
                .append(ClientOutput.INDENT).append(ClientOutput.INDENT)
                        .append("disallowed by rule: ")
                        .append(getKey()).append(" - ")
                        .append(getDescription()).append("\n")
                .toString();
    }
}
