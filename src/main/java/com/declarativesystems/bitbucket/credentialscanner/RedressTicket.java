package com.declarativesystems.bitbucket.credentialscanner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RedressTicket {

    private String key;
    private String resolution;
    private List issues;

    public RedressTicket(Map res) {
        // issues could be missing if the json returned was malformed...
        issues = (List) res.getOrDefault("issues", new ArrayList<>());

        if (issues.size() == 1) {
            Map ticket = (Map) issues.get(0);

            // resolution will be `null` on open tickets...
            Map resolutionField = (Map) ((Map) (ticket.get("fields"))).get("resolution");
            resolution = resolutionField == null ? null : (String) resolutionField.get("name");

            key = (String) ticket.get("key");
        }
    }

    public String getKey() {
        return key;
    }

    public String getResolution() {
        return resolution;
    }

    public int count() {
        return issues.size();
    }

    public String toString() {
        return String.format(
                "[%s] resolution: %s (issues count: %d) ",
                getKey(),
                getResolution(),
                count()
        );
    }

}
