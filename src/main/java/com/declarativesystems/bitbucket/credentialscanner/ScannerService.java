package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.repository.Repository;

import java.util.List;

public interface ScannerService {
    List<RuleViolation> scan(Repository repo, String commitId);
    int countCommits(Repository repo, String fromHash, String toHash);
}
