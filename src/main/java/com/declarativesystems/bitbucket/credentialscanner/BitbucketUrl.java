package com.declarativesystems.bitbucket.credentialscanner;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BitbucketUrl {

    private String project;
    private String slug;
    private String filePath;
    private String revision;
    private boolean bitbucketUrl;
    private Matcher matcher;

    public String getProject() {
        return project;
    }

    public String getSlug() {
        return slug;
    }

    public String getFilePath() {
        return filePath;
    }


    public String getRevision() {
        return revision;
    }


    /**
     * /projects/RUL/repos/rules/raw/test.json
     * URLs must match this regex which we also use for field capture:
     * $1 - project key
     * $2 - slug/repository name
     * $3 - path
     * $4 - at parameter (all)
     * $5 - commit/branch
     */
    private static final Pattern regex = Pattern.compile(
            "^/projects/([^/]+)/repos/([^/]+)/raw/([^?]+)(\\?at=(.*))?$"
    );


    public BitbucketUrl(String baseUrl, String targetUrl) {
        bitbucketUrl = targetUrl.startsWith(baseUrl);
        if (bitbucketUrl) {
            parseBitbucketUrl(targetUrl.replace(baseUrl, ""));
        }


    }

    /**
     * should be left with something like
     * /projects/RUL/repos/rules/raw/test.json
     */
    private void parseBitbucketUrl(String path) {
        // only allow paths matching regex we expect
        matcher = regex.matcher(path);
        if (matcher.matches()) {
            project = matcher.group(1);
            slug = matcher.group(2);
            filePath = matcher.group(3);
            revision = matcher.group(5);

            if (revision != null) {
                try {
                    revision = URLDecoder.decode(
                            revision,
                            StandardCharsets.UTF_8.displayName()
                    );
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            }
        } else {
            throw new RuntimeException(
                "Access denied: only files from bitbucket repositories can be accessed"
            );
        }
    }

    public boolean isBitbucketUrl() {
        return bitbucketUrl && matcher.matches();
    }
}
