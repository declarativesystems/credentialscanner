package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.net.ResponseException;

import java.io.UnsupportedEncodingException;
import java.net.URI;

public interface JiraLookupService {
    boolean isJiraLinkAvailable();
    URI getPrimarJiraRpcUrl();
    RedressTicket lookupRedressTicket(String Project, String commitId) throws CredentialsRequiredException, ResponseException;
    String createRedressTicketUrl(String project, String commitid) throws CredentialsRequiredException, ResponseException, UnsupportedEncodingException;
    String projectId(String projectKey) throws CredentialsRequiredException, ResponseException;
    JiraOverrideRequest handleJiraOverrideRequest(boolean redressTicketsEnabled,
                                                  ClientOutput clientOutput,
                                                  String redressTicketProject,
                                                  String resolutionApprove,
                                                  String resolutionReject,
                                                  String commitId);
}
