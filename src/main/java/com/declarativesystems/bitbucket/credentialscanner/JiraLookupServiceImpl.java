package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Named
public class JiraLookupServiceImpl implements JiraLookupService {

    private Logger logger = LoggerFactory.getLogger(JiraLookupServiceImpl.class);

    private ApplicationLinkService applicationLinkService;

    @Inject
    public JiraLookupServiceImpl(@ComponentImport ApplicationLinkService applicationLinkService) {
        this.applicationLinkService = applicationLinkService;
    }

    private ApplicationLink getPrimaryJiraLink() {
        return applicationLinkService.getPrimaryApplicationLink(
                JiraApplicationType.class
        );
    }

    public URI getPrimarJiraRpcUrl() {
        return getPrimaryJiraLink().getRpcUrl();
    }

    public RedressTicket lookupRedressTicket(String project, String commitId) throws CredentialsRequiredException, ResponseException {
        String ticketSubject = summary(commitId);

        RedressTicket redressTicket = null;


        ApplicationLink applicationLink = getPrimaryJiraLink();
        if (applicationLink == null) {
            throw new RuntimeException("Ticket lookup failed: Applink to Jira not configured");
        } else {
            ApplicationLinkRequestFactory applicationLinkRequestFactory = applicationLink.createAuthenticatedRequestFactory();

            // JQL injection isn't a "thing" since each clause just adds
            // filters. Indeed the most insecure JQL statement is the empty
            // string which returns all user-accessible data. Each clause
            // added _reduces_ the security impact so it doesn't matter what
            // is added...
            // https://community.atlassian.com/t5/Jira-questions/JQL-input-sanitization/qaq-p/967393
            // https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/search-searchUsingSearchRequest

            ticketSubject = ticketSubject.replace("'", "''");
            String requestBody = "{\n" +
                    "    \"jql\": \"project = " + project + " and summary ~ \\\"" + ticketSubject + "\\\"\",\n" +
                    "    \"startAt\": 0,\n" +
                    "    \"maxResults\": 2,\n" +
                    "    \"fields\": [\n" +
                    "        \"summary\",\n" +
                    "        \"resolution\",\n" +
                    "        \"assignee\"\n" +
                    "    ]\n" +
                    "}";
            // https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-auto-merge-pull-requests-when-conditions-are-met/qaq-p/212032
            String target = applicationLink.getRpcUrl() + "/rest/api/2/search";
            logger.debug("Requesting " + target + "\n" + requestBody);
            Map res = applicationLinkRequestFactory
                    .createRequest(Request.MethodType.POST, target)
                    .addHeader("Content-Type", "application/json")
                    .setRequestBody(requestBody)
                    .executeAndReturn(response -> {
                        logger.debug("processing response");
                        if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
                            String message = "Error " +response.getStatusCode() + ": " + target;
                            logger.error(message);
                            throw new ResponseException(message);
                        }

                        Gson gson = new GsonBuilder().create();
                        logger.debug("request made..parsing: " + response.getResponseBodyAsString());
                        return gson.fromJson(response.getResponseBodyAsString(), Map.class);

                    });
            logger.debug("returned returned");
            RedressTicket redressTicketResult = new RedressTicket(res);
            int issuesCount = redressTicketResult.count();
            logger.debug("Redress ticket from Jira: " + redressTicketResult.toString());
            if (issuesCount == 1) {
                redressTicket = redressTicketResult;
                return redressTicket;
            } else if (issuesCount > 1) {
                throw new RuntimeException(
                        "Too many issues matched \"" + ticketSubject + "\""
                );
            }
        }
        return redressTicket;
    }

    // https://jira.atlassian.com/browse/JRASERVER-25092
    // https://jira.atlassian.com/browse/JRASERVER-66244
    private String summary(String commitId) {
        String tag = "[credentialscanner override]";
        return tag + " " + commitId;
    }

    /**
     * Resolve project name eg `TEST` to an ID eg `10000`. This is needed as the
     * create ticket link only accepts project IDs
     * @param projectKey Project key to lookup
     * @return corresponding project id
     */
    public String projectId(String projectKey) throws CredentialsRequiredException, ResponseException {
        if (StringUtils.isBlank(projectKey)) {
            throw new RuntimeException("projectKey missing from lookup");
        }
        ApplicationLink applicationLink = getPrimaryJiraLink();

        String projectId;
        if (applicationLink == null) {
            throw new RuntimeException("Project ID lookup failed: Applink to Jira not configured");
        } else {
            ApplicationLinkRequestFactory applicationLinkRequestFactory = applicationLink.createAuthenticatedRequestFactory();

            // https://community.atlassian.com/t5/Bitbucket-questions/Bitbucket-auto-merge-pull-requests-when-conditions-are-met/qaq-p/212032
            String target = applicationLink.getRpcUrl() + "/rest/api/2/project/" + projectKey;
            logger.debug("Requesting " + target);

            Map res = applicationLinkRequestFactory
                    .createRequest(Request.MethodType.GET, target)
                    .addHeader("Content-Type", "application/json")
                    .executeAndReturn(response -> {
                        if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
                            throw new ResponseException(response.getResponseBodyAsString());
                        }

                        Gson gson = new GsonBuilder().create();
                        logger.debug("request made..parsing: " + response.getResponseBodyAsString());
                        return gson.fromJson(response.getResponseBodyAsString(), Map.class);
                    });

                projectId = (String) res.get("id");
        }

        return projectId;
    }

    @Override
    public String createRedressTicketUrl(String project, String commitId) throws CredentialsRequiredException, ResponseException, UnsupportedEncodingException {
        ApplicationLink applicationLink = applicationLinkService.getPrimaryApplicationLink(
                JiraApplicationType.class
        );

        String createTicketUrl = null;
        if (applicationLink != null) {

            // 10000 - epic
            // 10100 - improvement
            // scrape from GET/project api call

            // "&issuetype=10100" <== how to set issue type
            createTicketUrl = applicationLink.getDisplayUrl() +
                    "/secure/CreateIssueDetails!init.jspa?" +
                    "pid=" + projectId(project) +
                    "&issuetype=10104" +
                    "&summary=" + URLEncoder.encode(summary(commitId), StandardCharsets.UTF_8.toString());

        }
        return createTicketUrl;
    }

    @Override
    public boolean isJiraLinkAvailable() {
        return (getPrimaryJiraLink() != null);
    }

    @Override
    public JiraOverrideRequest handleJiraOverrideRequest(boolean redressTicketsEnabled,
                                                         ClientOutput clientOutput,
                                                         String redressTicketProject,
                                                         String resolutionApprove,
                                                         String resolutionReject,
                                                         String commitId) {
        JiraOverrideRequest jiraOverrideRequest = new JiraOverrideRequest();

        if (redressTicketsEnabled && isJiraLinkAvailable()) {

            // final sanity check that approved/rejected resolution good
            if (StringUtils.isBlank(resolutionApprove) ||
                    StringUtils.isBlank(resolutionReject) ||
                    resolutionApprove.equals(resolutionReject)) {
                throw new RuntimeException(
                        "Invalid jira integration settings detected in" +
                                "credentialscanner, bad value(s) for " +
                                "resolutionApprove: " + resolutionApprove +
                                "resolutionReject: " + resolutionReject
                );

            }

            try {
                RedressTicket redressTicket = lookupRedressTicket(
                        redressTicketProject,
                        commitId
                );
                if (redressTicket == null) {
                    // no such ticket
                    jiraOverrideRequest.setMessage(
                            "Create a JIRA ticket and mark resolved \"" +
                                    resolutionApprove + "\" to dismiss this error: " +
                                    createRedressTicketUrl(
                                            redressTicketProject,
                                            commitId
                                    )
                    );
                } else if (redressTicket.getResolution() == null) {
                    // open ticket (to do)
                    jiraOverrideRequest.setMessage(
                            "Mark ticket " + redressTicket.getKey() +
                                    " resolved \"" + resolutionApprove +
                                    "\" to dismiss this error"
                    );
                } else if (redressTicket.getResolution().equalsIgnoreCase(resolutionApprove)) {
                    // approved
                    clientOutput.println(
                            1,
                            "credentialscanner [APPROVED/",
                            redressTicket.getKey(),
                            "] - ",
                            commitId
                    );
                    jiraOverrideRequest.setOverride(true);
                } else if (redressTicket.getResolution().equalsIgnoreCase(resolutionReject)) {
                    // rejected
                    jiraOverrideRequest.setMessage(
                            "This commit was rejected by " +
                                    redressTicket.getKey() +
                                    " please rework your changes"
                    );
                } else {
                    throw new RuntimeException("Invalid redress ticket state: " + redressTicket.toString());
                }
            } catch (CredentialsRequiredException e) {
                String message =
                        "Bad credentials for Jira (check your Application Link bitbucket->jira): "
                                + e.getMessage();
                logger.error(message);
                jiraOverrideRequest.setMessage(message);
            } catch (ResponseException e) {
                String message =
                        "ResponseError from Jira server at: "
                                + getPrimarJiraRpcUrl()
                                + " - " + e.getMessage();
                logger.error(message);
                jiraOverrideRequest.setMessage(message);
            } catch (UnsupportedEncodingException e) {
                String message =
                        "UnsupportedEncoding generating create ticket URL:"
                                + e.getMessage();
                logger.error(message);
                jiraOverrideRequest.setMessage(message);
            } catch (RuntimeException e) {
                String message =
                        "Error searching Jira: "
                                + e.getMessage();
                logger.error(message);
                jiraOverrideRequest.setMessage(message);
            }
        } else {
            jiraOverrideRequest.setMessage(
                    "Redress tickets disabled or Jira Application Link missing"
            );
        }
        return jiraOverrideRequest;
    }
}
