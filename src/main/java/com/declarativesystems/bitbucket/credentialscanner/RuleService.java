package com.declarativesystems.bitbucket.credentialscanner;

import java.util.Map;

public interface RuleService {
    Ruleset loadAndValidateRuleset(String customRulesUrl);

    /**
     * Load rules by requesting and parsing JSON from this URL
     * @param customRulesUrl URL to load rule from or null to use built-in rules
     */
    void loadRuleset(String customRulesUrl);

    /**
     * Get all rules
     * @return `Map` of all rules
     */
    Ruleset getRuleset();

    /**
     * Get rules that are applicable for a given file
     * @param filename filename to which rules should apply
     * @return filtered list of rules
     */
    Map<String, Rule> getRulesForFile(String filename);

    long getRuleDownloadTimeMs();

    long getRuleProcessTimeMs();

    long getRuleTotalTimeMs();

    void setRuleset(Ruleset rules);
}
