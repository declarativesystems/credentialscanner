package com.declarativesystems.bitbucket.credentialscanner;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Representation of a rule as parsed from JSON. The setters dont seem to be
 * used by GSON - it appears to use introspection(?). They *are* needed by our
 * own tests though(!)... dont try to add code to the setters it wont work.
 */
public class Rule {
    /**
     * Files this rule applies to (shell glob) can include directory and/or
     * filename eg `/src/*`, `*.php`, `/src/main/java/*.java`
     */
    private String filePattern = null;

    /**
     * Files this rule does not apply to (shell glob) can include a directory
     * and/or filename `/src/test/*`, `*test*.pem.php`
     */
    private String excludedPattern = null;

    /**
     * Regular expression to match with
     */
    private String regex = null;

    /**
     * We need a real `Matcher` to do an un-anchored regex scan and we need a
     * `Pattern` to make one. Keep it in memory for speed.
     * `transient` is to prevent GSON errors trying to de-serialise
     */
    private transient Pattern pattern = null;

    /**
     * Human readable rule description
     */
    private String description = null;

    public String getFilePattern() {
        return filePattern;
    }

    public void setFilePattern(String filePattern) {
        this.filePattern = filePattern;
    }

    public String getExcludedPattern() {
        return excludedPattern;
    }

    public void setExcludedPattern(String excludedPattern) {
        this.excludedPattern = excludedPattern;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Test whether the text matches the rule's regex. The built-in
     * `String.match()` method is anchored by default so use a Matcher for
     * simpler rules. More info: https://stackoverflow.com/a/1894630/3441106
     * @param text Text to apply pattern to
     * @return True if the text matches regex otherwise false
     */
    public boolean matches(String text) {
        return pattern.matcher(text).find();
    }

    public boolean ruleApplies(String filename) {
        return
                ! FilenameUtils.wildcardMatch(filename, excludedPattern)
                && FilenameUtils.wildcardMatch(filename, filePattern);
    }

    public boolean isValid() {
        return getFilePattern() != null
                && getDescription() != null
                && isValidRegex();
    }

    public boolean isValidRegex() {
        boolean valid = false;

        if (StringUtils.isNotBlank(getRegex())) {
            try {
                pattern = Pattern.compile(getRegex());
                valid = true;
            } catch (PatternSyntaxException ignored) {
            }
        }
        return valid;
    }

    /**
     * All fields are required. Report the fields that are bad/empty/missing
     * @return List of bad fields
     */
    public List<String> getBadFields() {
        ArrayList<String> badFields = new ArrayList<>();

        if (getFilePattern() == null) {
            badFields.add("filePattern");
        }

        if (getDescription() == null) {
            badFields.add("description");
        }

        if (! isValidRegex()) {
            badFields.add("regex");
        }

        return badFields;
    }

}
