package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.hook.repository.*;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.declarativesystems.atlassian.EntitlementCheck;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.PrintWriter;

@Named
public class CredentialScannerPreReceiveRepositoryHook implements PreRepositoryHook<RepositoryPushHookRequest>, SettingsValidator
{

    private ScannerService scannerService;
    private JiraLookupService jiraLookupService;
    private RuleService ruleService;
    private EntitlementCheck entitlementCheck;

    public static final String VERBOSE_ENABLED = "verboseEnabled";
    public static final String REDRESS_TICKETS_ENABLED = "redressTicketsEnabled";
    public static final String REDRESS_TICKET_PROJECT_BOX = "redressTicketProjectBox";
    public static final String RESOLUTION_APPROVE_BOX = "resolutionApproveBox";
    public static final String RESOLUTION_REJECT_BOX = "resolutionRejectBox";
    public static final String CUSTOM_RULES_ENABLED    = "customRulesEnabled";
    public static final String CUSTOM_RULES_URL = "customRulesUrl";
    public static final String NO_SELECTION = "select...";

    @Inject
    public CredentialScannerPreReceiveRepositoryHook(ScannerService scannerService,
                                                     JiraLookupService jiraLookupService,
                                                     RuleService ruleService,
                                                     EntitlementCheck entitlementCheck) {
        this.scannerService = scannerService;
        this.jiraLookupService = jiraLookupService;
        this.ruleService = ruleService;
        this.entitlementCheck = entitlementCheck;
    }

    private void licenseCheck(ClientOutput clientOutput) {
        String licenseStatus = entitlementCheck.getLicenseMessage();

        if (licenseStatus != null) {
            clientOutput.println(
                    0,
                    "\n*** ",
                    licenseStatus,
                    " ***\n"
            );
        }
    }

    /**
     * https://community.atlassian.com/t5/Bitbucket-questions/Reading-the-contents-of-a-file-in-a-pre-commit-hook-filter/qaq-p/662321#M28021
     */
    @Nonnull
    @Override
    public RepositoryHookResult preUpdate(@Nonnull PreRepositoryHookContext preRepositoryHookContext, @Nonnull RepositoryPushHookRequest repositoryPushHookRequest) {
        // defer to CredentialScannerCallback for the real status unless error
        RepositoryHookResult repositoryHookResult = RepositoryHookResult.accepted();
        PrintWriter out = repositoryPushHookRequest.getScmHookDetails().get().out();

        //
        // atlassian settings lookup
        //
        Settings settings = preRepositoryHookContext.getSettings();
        boolean verboseEnabled = settings.getBoolean(
                CredentialScannerPreReceiveRepositoryHook.VERBOSE_ENABLED,
                false
        );

        boolean customRules = settings.getBoolean(CUSTOM_RULES_ENABLED, false);
        String customRulesUri = customRules ?
                settings.getString(CUSTOM_RULES_URL, "") : null;

        boolean redressTicketsEnabled = settings.getBoolean(
                CredentialScannerPreReceiveRepositoryHook.REDRESS_TICKETS_ENABLED,
                false
        );
        String redressTicketProject = settings.getString(
                CredentialScannerPreReceiveRepositoryHook.REDRESS_TICKET_PROJECT_BOX,
                ""
        );
        String resolutionApprove = settings.getString(
                CredentialScannerPreReceiveRepositoryHook.RESOLUTION_APPROVE_BOX,
                ""
        );
        String resolutionReject  = settings.getString(
                CredentialScannerPreReceiveRepositoryHook.RESOLUTION_REJECT_BOX,
                ""
        );

        ClientOutput clientOutput = new ClientOutput(out, verboseEnabled);
        licenseCheck(clientOutput);

        // load the rules once at start of hook
        ruleService.loadRuleset(customRulesUri);

        // single instance PER REQUEST
        CredentialScannerCallback credentialScannerCallback = new CredentialScannerCallback(
                clientOutput,
                repositoryPushHookRequest.getRepository(),
                scannerService,
                jiraLookupService,
                redressTicketsEnabled,
                redressTicketProject,
                resolutionApprove,
                resolutionReject
        );
        // each *BRANCH* in this request
        for (RefChange refChange : repositoryPushHookRequest.getRefChanges()) {
            clientOutput.verbose(
                    0,
                    "Branch: ",
                    refChange.getRef().getDisplayId()
            );

            int commitCount = scannerService.countCommits(
                    repositoryPushHookRequest.getRepository(),
                    refChange.getFromHash(),
                    refChange.getToHash()
            );

            if (commitCount >= 1000) {
                // redress ticket in jira?
                JiraOverrideRequest jiraOverrideRequest = jiraLookupService.handleJiraOverrideRequest(
                        redressTicketsEnabled,
                        clientOutput,
                        redressTicketProject,
                        resolutionApprove,
                        resolutionReject,
                        refChange.getToHash()
                );

                repositoryHookResult = jiraOverrideRequest.isOverride() ?
                    RepositoryHookResult.accepted() :
                    RepositoryHookResult.rejected(
                            "credentialscanner [REJECTED] (too many commits) - " + refChange.getToHash(),
                            jiraOverrideRequest.getMessage()
                    )
                ;

            } else {
                // each *COMMIT* has to be processed by a callback!
                preRepositoryHookContext.registerCommitCallback(
                        credentialScannerCallback,
                        RepositoryHookCommitFilter.ADDED_TO_REPOSITORY
                );
            }
        }

        return repositoryHookResult;
    }

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors settingsValidationErrors, @Nonnull Scope scope) {
        if (settings.getBoolean(CUSTOM_RULES_ENABLED, false)) {
            String customRulesUri = settings.getString(CUSTOM_RULES_URL,"");
            if (StringUtils.isBlank(customRulesUri)) {
                settingsValidationErrors.addFieldError(
                    CUSTOM_RULES_URL,
                    "Custom rules URL is required when custom rules enabled"
                );
            } else {

                try {
                    ruleService.loadAndValidateRuleset(customRulesUri);
                } catch (RuntimeException e) {
                    settingsValidationErrors.addFieldError(
                            CUSTOM_RULES_URL,
                            e.getMessage()
                    );
                }
            }
        }

        if (settings.getBoolean(REDRESS_TICKETS_ENABLED, false)) {
            // project
            String project = settings.getString(REDRESS_TICKET_PROJECT_BOX, "");
            String approve = settings.getString(RESOLUTION_APPROVE_BOX, "");
            String reject = settings.getString(RESOLUTION_REJECT_BOX, "");

            if (StringUtils.isBlank(project) || project.equals(NO_SELECTION)) {
                settingsValidationErrors.addFieldError(
                        REDRESS_TICKET_PROJECT_BOX,
                        "Must select a jira project"
                );
            }

            if (StringUtils.isBlank(approve) || approve.equals(NO_SELECTION)) {
                settingsValidationErrors.addFieldError(
                        RESOLUTION_APPROVE_BOX,
                        "Must select resolution to mark approval"
                );
            }

            if (StringUtils.isBlank(reject) || reject.equals(NO_SELECTION)) {
                settingsValidationErrors.addFieldError(
                        RESOLUTION_REJECT_BOX,
                        "Must select resolution to mark rejection"
                );
            }

            if (StringUtils.isNotBlank(approve) &&
                    StringUtils.isNotBlank(reject) &&
                    approve.equals(reject)) {

                settingsValidationErrors.addFieldError(
                        RESOLUTION_REJECT_BOX,
                        "Resolution to mark rejection must be different to resolution to mark approval"
                );
            }
        }
    }
}
