package com.declarativesystems.bitbucket.credentialscanner;

public class JiraOverrideRequest {
    private boolean override = false;
    private String message = "";

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
