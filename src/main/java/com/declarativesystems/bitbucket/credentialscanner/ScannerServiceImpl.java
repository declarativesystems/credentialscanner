package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
public class ScannerServiceImpl implements ScannerService {

    private Logger logger = LoggerFactory.getLogger(ScannerServiceImpl.class);
    private GitCommandBuilderFactory gitCommandBuilderFactory;
    private RuleService ruleService;

    @Inject
    public ScannerServiceImpl(@ComponentImport GitCommandBuilderFactory gitCommandBuilderFactory,
                              RuleService ruleService) {
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
        this.ruleService = ruleService;
    }

    public int countCommits(Repository repo, String fromHash, String toHash) {

        String hashRange = fromHash.equals("0000000000000000000000000000000000000000") ?
                toHash : fromHash + ".." + toHash;

        logger.debug(
                "Running external git to count revisions in range {}...",
                hashRange
        );

        GitOnelinerOutputHandler gitOnelinerOutputHandler = new GitOnelinerOutputHandler();
        gitCommandBuilderFactory.builder(repo)
                .command("rev-list").argument("--count").argument(hashRange)
                .build(gitOnelinerOutputHandler)
                .call();

        int commitCount = Integer.parseInt(
                gitOnelinerOutputHandler.getOutput()
                        .get(0)
        );
        logger.debug("Result...{}", commitCount);
        return commitCount;
    }

    //https://community.atlassian.com/t5/Answers-Developer-Questions/git-shortlog/qaq-p/527213
    public List<RuleViolation> scan(Repository repo, String commitId) {
        Date start = new Date();
        GitCommandOutputHandler gitCommandOutputHandler =
                new GitCommandOutputHandler(ruleService);

        // running git directly seems the only sensible way to get the raw patch
        // output. there is a programatic API but it imposes hard limits on how
        // much data can be returned and does not allow paging:
        // https://docs.atlassian.com/bitbucket-server/javadoc/5.8.0/api/reference/com/atlassian/bitbucket/commit/CommitService.html
        // bitbucket will evaluate commits newest->oldest
        //logger.debug("running git log -p -n 1 {}...", commitId);
        gitCommandBuilderFactory.builder(repo)
                .command("log").argument("-p").argument("-n").argument("1").argument(commitId)
                .build(gitCommandOutputHandler).call();


        Date end = new Date();
        long duration = end.getTime() - start.getTime();
        return (List<RuleViolation>) gitCommandOutputHandler.getOutput();
    }
}
