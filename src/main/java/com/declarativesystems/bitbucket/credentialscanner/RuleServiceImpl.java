package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Named
public class RuleServiceImpl implements RuleService {

    private Logger logger = LoggerFactory.getLogger(RuleServiceImpl.class);

    private Ruleset ruleset;
    private Map<String, Rule> invalidRules;
    private ApplicationProperties applicationProperties;
    private RequestFactory requestFactory;

    private ContentService contentService;
    private RepositoryService repositoryService;

    private long startDownloadRules = -1;
    private long endDownloadRules = -1;
    private long endProcessRules = -1;



    @Inject
    public RuleServiceImpl(@ComponentImport ApplicationProperties applicationProperties,
                           @ComponentImport RequestFactory requestFactory,
                           @ComponentImport ContentService contentService,
                           @ComponentImport RepositoryService repositoryService) {
        this.applicationProperties = applicationProperties;
        this.requestFactory = requestFactory;

        this.contentService = contentService;
        this.repositoryService = repositoryService;
    }

    @Override
    public Ruleset getRuleset() {
        return ruleset;
    }

    public Map<String, Rule> getInvalidRules() {
        return invalidRules;
    }

    /**
     * Return the sub-set rules that should be used to scan a particular file,
     * eg if you have 10 rules loaded running this function might show that you
     * only need to process 3 rules for the file that you want to check
     */
    @Override
    public Map<String, Rule> getRulesForFile(String filename) {

        return (ruleset.isSettingsIgnore(filename)) ?
            new HashMap<>() :
            // https://beginnersbook.com/2017/10/java-8-filter-a-map-by-keys-and-values/
            ruleset.getRules().entrySet()
                    .stream()
                    .filter(map -> map.getValue().ruleApplies(filename))
                    .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
    }

    public Ruleset loadAndValidateRules(InputStream in) throws IOException {
        Gson gson = new Gson();
        String raw;
        raw = IOUtils.toString(in, StandardCharsets.UTF_8.toString());


        logger.debug("JSON RECEIVED:\n" + raw);
        endDownloadRules = new Date().getTime();

        // https://futurestud.io/tutorials/gson-mapping-of-maps
        Ruleset allRules = gson.fromJson(raw, Ruleset.class);
        invalidRules = allRules.getRules().entrySet()
                .stream()
                .filter(map -> ! map.getValue().isValid())
                .collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));

        if (invalidRules.size() > 0) {
            String invalidRuleKeys = String.join(", ",
                    invalidRules.keySet().toArray(new String[invalidRules.size()])
            );

            throw new RuntimeException(
                    "Error loading credentialscanner rules: " +
                    invalidRules.size() + " invalid rule(s) detected, " +
                    "offending keys: " + invalidRuleKeys);
        }

        endProcessRules = new Date().getTime();

        return allRules;
    }

    /**
     * Get the input stream to read the custom URL with. If it belongs to our
     * bitbucket server, then we can use applinks/requests to download the file
     * which will allow access to private repositories, otherwise the file must
     * be publicly available
     * @param customRuleUrl URL to download from
     * @return Opened input stream
     */
    private InputStream getInputStreamForUrl(String customRuleUrl) {
        String baseUrl = applicationProperties.getBaseUrl(UrlMode.ABSOLUTE);

        BitbucketUrl bitbucketUrl = new BitbucketUrl(baseUrl, customRuleUrl);
        InputStream in;
        if (bitbucketUrl.isBitbucketUrl()) {
            // http://localhost:7990/bitbucket/projects/RUL/repos/rules/raw/test.json
            Repository repository = repositoryService.getBySlug(
                    bitbucketUrl.getProject(),
                    bitbucketUrl.getSlug()
            );

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            contentService.streamFile(
                    repository,
                    bitbucketUrl.getRevision(),
                    bitbucketUrl.getFilePath(),
                    (s) -> out
            );

            in = new ByteArrayInputStream( out.toByteArray() );
        } else {
           logger.debug("Requesting " + customRuleUrl);

            try {
            in = (InputStream) requestFactory
                    .createRequest(Request.MethodType.GET, customRuleUrl)
                        .addHeader("Content-Type", "application/json")
                        .executeAndReturn(response -> {
                            if (response.getStatusCode() != HttpURLConnection.HTTP_OK) {
                                String message = "Error " + response.getStatusCode() + ": " + customRuleUrl;
                                throw new ResponseException(message);
                            }
                            logger.debug("got data... getting inputstream");
                            return response.getResponseBodyAsStream();
                        });
            } catch (ResponseException e) {
                throw new RuntimeException(e.getMessage());
            }
        }

        return in;
    }

    @Override
    public Ruleset loadAndValidateRuleset(String customRulesUrl) {
        startDownloadRules = new Date().getTime();
        try (InputStream in = (customRulesUrl == null) ?
                getClass().getResourceAsStream("/default_rules.json") :
                getInputStreamForUrl(customRulesUrl)) {

            return loadAndValidateRules(in);
        } catch (IOException e) {
            throw new RuntimeException("Error loading rules: " + e.getMessage());
        }
    }

    @Override
    public void loadRuleset(String customRulesUrl) {
        ruleset = loadAndValidateRuleset(customRulesUrl);
    }

    /**
     * Test support
     * @param in Inputstream to read ruleset from
     */
    public void loadRuleset(InputStream in) throws IOException {
        ruleset = loadAndValidateRules(in);
    }

    @Override
    public long getRuleDownloadTimeMs() {
        return endDownloadRules - startDownloadRules;
    }

    @Override
    public long getRuleProcessTimeMs() {
        return endProcessRules - endDownloadRules;
    }

    @Override
    public long getRuleTotalTimeMs() {
        return endProcessRules - startDownloadRules;
    }

    @Override
    public void setRuleset(Ruleset ruleset) {
        this.ruleset = ruleset;
    }
}
