package com.declarativesystems.bitbucket.credentialscanner;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class GitOnelinerOutputHandler implements CommandOutputHandler<List<String>> {
    private List<String> capture;

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        try {
            capture = IOUtils.readLines(inputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new ProcessException(e.getMessage());
        }
    }

    @Override
    public void complete() {

    }

    @Override
    public void setWatchdog(Watchdog watchdog) {

    }

    @Nullable
    @Override
    public List<String> getOutput() {
        return capture;
    }
}
