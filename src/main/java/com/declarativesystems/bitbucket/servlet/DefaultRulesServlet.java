package com.declarativesystems.bitbucket.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.io.IOUtils;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class DefaultRulesServlet extends HttpServlet{

    private UserManager userManager;

    @Inject
    public DefaultRulesServlet(@ComponentImport final UserManager userManager) {
        this.userManager = userManager;
    }

    private boolean permission_denied() {
        UserKey userKey = userManager.getRemoteUserKey();
        return userKey == null || !userManager.isAdmin(userKey);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        if (permission_denied()) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            resp.setContentType("application/json");

            InputStream in = getClass().getResourceAsStream("/default_rules.json");
            try {
                resp.getWriter().write(
                        IOUtils.toString(in, StandardCharsets.UTF_8.toString())
                );
                resp.setStatus(HttpServletResponse.SC_OK);
            } catch (IOException e) {
                throw new RuntimeException(
                        "IOException reading rules: " + e.getMessage());
            }
        }
    }
}