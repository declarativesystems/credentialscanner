/*
 * Copyright 2017 Declarative Systems PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.declarativesystems.bitbucket.servlet;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.declarativesystems.atlassian.EntitlementCheck;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LicenseServlet extends HttpServlet {

    private UserManager userManager;
    private EntitlementCheck entitlementCheck;

    @Inject
    public LicenseServlet(@ComponentImport UserManager userManager,
                          EntitlementCheck entitlementCheck) {
        this.userManager = userManager;
        this.entitlementCheck = entitlementCheck;
    }

    private boolean permission_denied() {
        UserKey userKey = userManager.getRemoteUserKey();
        return userKey == null || !userManager.isAdmin(userKey);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (permission_denied()) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            // Set standard HTTP/1.1 no-cache headers.
            resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

            // Set standard HTTP/1.0 no-cache header.
            resp.setHeader("Pragma", "no-cache");

            resp.getWriter().print(entitlementCheck.getLicenseKey());
            resp.setStatus(HttpServletResponse.SC_OK);
        }
    }
}
