package com.declarativesystems.cli;

import com.declarativesystems.bitbucket.credentialscanner.Rule;
import com.declarativesystems.bitbucket.credentialscanner.RuleService;
import com.declarativesystems.bitbucket.credentialscanner.RuleServiceImpl;
import com.declarativesystems.bitbucket.credentialscanner.Ruleset;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class LocalFileRulesService implements RuleService {
    private Map<String, Rule> rules;
    private Map<String, Rule> invalidRules;
    private RuleServiceImpl ruleServiceImpl;

    public LocalFileRulesService() {
        rules = new HashMap<>();
        invalidRules = new HashMap<>();
        ruleServiceImpl = new RuleServiceImpl(
                null,
                null,
                null,
                null
        );

    }

    @Override
    public Ruleset loadAndValidateRuleset(String customRulesUrl) {
        try {
            InputStream in = (customRulesUrl == null) ?
                    getClass().getResourceAsStream("/default_rules.json") :
                    new FileInputStream(customRulesUrl);

            return ruleServiceImpl.loadAndValidateRules(in);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File not found loading rules: "  + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException("Error loading rules: " + e.getMessage());
        }
    }


    @Override
    public void loadRuleset(String customRulesUrl) {
        ruleServiceImpl.setRuleset(loadAndValidateRuleset(customRulesUrl));
    }

    @Override
    public Ruleset getRuleset() {
        return ruleServiceImpl.getRuleset();
    }

    @Override
    public Map<String, Rule> getRulesForFile(String filename) {
        return ruleServiceImpl.getRulesForFile(filename);
    }

    @Override
    public long getRuleDownloadTimeMs() {
        return ruleServiceImpl.getRuleDownloadTimeMs();
    }

    @Override
    public long getRuleProcessTimeMs() {
        return ruleServiceImpl.getRuleProcessTimeMs();
    }

    @Override
    public long getRuleTotalTimeMs() {
        return ruleServiceImpl.getRuleTotalTimeMs();
    }

    @Override
    public void setRuleset(Ruleset rules) {
        ruleServiceImpl.setRuleset(rules);
    }
}
