package com.declarativesystems.cli;

import ch.qos.logback.classic.Level;
import com.atlassian.utils.process.ProcessException;
import com.declarativesystems.bitbucket.credentialscanner.GitCommandOutputHandler;
import com.declarativesystems.bitbucket.credentialscanner.GitOnelinerOutputHandler;
import com.declarativesystems.bitbucket.credentialscanner.RuleService;
import com.declarativesystems.bitbucket.credentialscanner.RuleViolation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class Exe {
    private static final String DEBUG_MODE = "--debug";

    private Logger logger = LoggerFactory.getLogger(Exe.class);
    private boolean debug = false;
    private File workingDir;
    private String revList;



    public static void main(String[] args) throws IOException, ProcessException {
        Exe exe = new Exe();
        exe.run(args);
    }

    private InputStream runCommand(String cmd, File workingDir) throws IOException {
        //
        logger.debug("Running {} in {}", cmd, workingDir.getAbsolutePath());
        Process p = Runtime.getRuntime().exec(cmd, null, workingDir);

        return p.getInputStream();
    }

    private void setupLogging() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        Level level = debug ? Level.DEBUG : Level.INFO;
        root.setLevel(level);
    }

    private void usage() {
        logger.info("USAGE: java -cp \"target/credentialscanner-1.0.0-SNAPSHOT.jar:target/bitbucket/app/WEB-INF/lib/*\" com.declarativesystems.cli.Exe DIRECTORY [REVLIST] ["+DEBUG_MODE+"]");
    }

    /**
     * args[0] - directory to scan
     * args[1] - revlist (eg FROMHASH..TOHASH or TOHASH (optional)
     */
    private void parseCommandLine(String[] args) {
        // lists from `Arrays.asList` are immutable-ish
        List<String> argsList = new ArrayList<>(Arrays.asList(args));
        if (argsList.contains(DEBUG_MODE)) {
            debug = true;
            argsList.remove(DEBUG_MODE);
        }

        if (argsList.size() < 1 || argsList.size() > 2) {
            usage();
            throw new RuntimeException("incorrect usage - wrong parameter count");
        }

        String filename = argsList.get(0);
        workingDir = new File(filename);
        if (! workingDir.exists()) {
            throw new RuntimeException("Directory does not exist: " + filename);
        }

        if (argsList.size() == 2) {
            // to hash only
            revList = argsList.get(1);
        } else {
            revList = "HEAD";
        }
    }

    private void run(String[] args) throws IOException, ProcessException {
        parseCommandLine(args);
        setupLogging();
        logger.info("Starting...");

        // load default rules
        RuleService ruleService = new LocalFileRulesService();
        ruleService.loadRuleset(null);

        logger.info("Rules loaded: {}", ruleService.getRuleset().getRules().size());

        // now get the in-order list of commits (reset)
        String cmd = "git rev-list " + revList;
        logger.debug("running: {}", cmd);
        GitOnelinerOutputHandler gitOnelinerOutputHandler = new GitOnelinerOutputHandler();
        gitOnelinerOutputHandler.process(
                runCommand(
                        cmd,
                        workingDir
                )
        );
        List<String> commits = gitOnelinerOutputHandler.getOutput();
        GitCommandOutputHandler gitCommandOutputHandler = new GitCommandOutputHandler(
                ruleService
        );
        List<RuleViolation> ruleViolations = new ArrayList<>();

        long metric1 = new Date().getTime();
        int progress = 0;
        for (int i = 0; i < commits.size() ; i++) {
            cmd = "git log -p -n 1 " + commits.get(i);
            gitCommandOutputHandler.process(
                runCommand(cmd , workingDir)
            );
            ruleViolations.addAll(gitCommandOutputHandler.getOutput());

            float pc = (float) i/commits.size() * 100;
            if (pc % 1 == 0) {
                long metric2 = new Date().getTime();
                long period = metric2 - metric1;
                metric1 = metric2;
                int processed = i - progress;
                progress = i;

                float commitsPerSeconds = ((float) processed / period * 1000);


                logger.info(
                        "{}% complete, @ {} commits/second",
                        pc,
                        commitsPerSeconds
                );
            }
        }
        logger.info("processed {}", commits.size());
        for (RuleViolation ruleViolation: ruleViolations) {
            logger.info(ruleViolation.toString());
        }
    }
}
