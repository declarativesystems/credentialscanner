package com.declarativesystems.atlassian;

import java.util.HashMap;

public interface EntitlementCheck {
    String UNLICENSED           = "unlicensed";
    String INVALID              = "invalid";
    String LICENSED             = "licensed";
    HashMap<String,String> LICENSE_MESSAGE = new HashMap<String,String>(){{
        put(UNLICENSED, "Please purchase a credentialscanner plugin license.");
        put(INVALID,    "Invalid license: Your evaluation license of credentialscanner plugin expired. Please purchase a new license.");        put(LICENSED,   null);
    }};

    /**
     * Key of our license state
     * @return one of the above constants which can be used as a lookup key in
     * `LICENSE_MESSAGE`
     */
    String getLicenseKey();

    /**
     * human readable version of key
     */
    String getLicenseMessage();
}
