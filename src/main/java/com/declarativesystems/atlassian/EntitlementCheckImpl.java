package com.declarativesystems.atlassian;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;

import javax.inject.Inject;
import javax.inject.Named;


@Named
public class EntitlementCheckImpl implements EntitlementCheck {

    private PluginLicenseManager pluginLicenseManager;

    @Inject
    public EntitlementCheckImpl(@ComponentImport PluginLicenseManager pluginLicenseManager)
    {
        this.pluginLicenseManager = pluginLicenseManager;
    }

    public String getLicenseMessage() {
        return LICENSE_MESSAGE.getOrDefault(getLicenseKey(), null);
    }

    @Override
    public String getLicenseKey() {
        String status;
        if (pluginLicenseManager.getLicense().isDefined()) {
            PluginLicense license = pluginLicenseManager.getLicense().get();
            if (license.getError().isDefined()) {
                // handle license error scenario
                // (e.g., expiration or user mismatch)
                status = EntitlementCheck.INVALID;
             } else {
                // handle valid license scenario
                status = EntitlementCheck.LICENSED;
            }
        } else {
            // handle unlicensed scenario
            status = UNLICENSED;
        }

        return status;
    }
}
